using Xunit;

namespace RPG_create
{
    public class UnitTest1
    {
        [Fact]
        public void Test_level_at_Start()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actual = 1;

            Character hero = new Warrior();

            Assert.Equal(actual, hero.Level);
        }
        [Fact]
        public void Test_level_at_levelUp()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actual = 2;

            Character hero = new Warrior();
            hero.LevelUp();

            Assert.Equal(actual, hero.Level);
        }
        [Fact]
        public void Test_atributes_at_WarriorStart()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actualStr = 5;
            int actualDex = 2;
            int actualInt = 1;

            Character hero = new Warrior();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_MageStart()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actualStr = 1;
            int actualDex = 1;
            int actualInt = 8;

            Character hero = new Mage();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_RogueStart()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actualStr = 2;
            int actualDex = 6;
            int actualInt = 1;

            Character hero = new Rogue();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_RangerStart()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you create your hero
            /// </summary>
           
            int actualStr = 1;
            int actualDex = 7;
            int actualInt = 1;

            Character hero = new Ranger();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_Warrior_at_lvl2()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you level once
            /// </summary>
          
            int actualStr = 8;
            int actualDex = 4;
            int actualInt = 2;

            Character hero = new Warrior();
            hero.LevelUp();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_Mage_at_lvl2()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you level once
            /// </summary>
         
            int actualStr = 2;
            int actualDex = 2;
            int actualInt = 13;
            
            Character hero = new Mage();
            hero.LevelUp();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_Rogue_at_lvl2()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you level once
            /// </summary>
           
            int actualStr = 3;
            int actualDex = 10;
            int actualInt = 2;

            Character hero = new Rogue();
            hero.LevelUp();
                
            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_atributes_at_Ranger_at_lvl2()
        {
            /// <summary>
            ///  It tests if the correct attributes is applied once you level once
            /// </summary>
           
            int actualStr = 2;
            int actualDex = 12;
            int actualInt = 2;

            Character hero = new Ranger();
            hero.LevelUp();

            Assert.True(hero.Strength == actualStr && hero.Dexterity == actualDex && hero.Intelligence == actualInt);
        }
        [Fact]
        public void Test_equip_higher_level_weapon()
        {
            /// <summary>
            /// Throws exception if weapon too high level
            /// </summary>
            Character hero = new Warrior();
            Weapon testWeap = new Weapon("TestWeap", 1, 2, new WeaponAttributes(10, 2)) { Type=Weaponkind.Axe};
       
            Assert.Throws<InvalidWeaponLevelException>(() => Methods.CheckWeapon(hero, testWeap));
          
        }
        [Fact]
        public void Test_equip_higher_level_armor()
        {
            /// <summary>
            /// Throws exception if armor too high level
            /// </summary>
            Character hero = new Warrior();
            Armor testArmor = new Armor("TestArmor", 1, 3, new ArmorAttributes(0, 2, 0)) { Type=ArmorType.Plate};
            Assert.Throws<InvalidArmorLevelException>(() => Methods.CheckArmor(hero,testArmor));

        }
        [Fact]
        public void Test_equip_wrong_weapon_warrior_equiping_staff()
        {
            /// <summary>
            /// Throws exception if weapon is of wrong type
            /// </summary>
            Character hero = new Warrior();
            Weapon testWeap = new Weapon("TestWeap", 1, 1, new WeaponAttributes(10, 2)) { Type = Weaponkind.Staff, Place = ItemPlace.Weapon };

            Assert.Throws<InvalidWeaponTypeException>(() => Methods.CheckWeapon(hero, testWeap));
        }
        [Fact]
        public void Test_equip_wrong_armor_warrior_equiping_cloth()
        {
            /// <summary>
            /// Throws exception if armor of wrong type 
            /// </summary>
            Character hero = new Warrior();
            Armor testArmor = new Armor("TestArmor", 1, 3, new ArmorAttributes(0, 2, 0)) { Type = ArmorType.Cloth, Place = ItemPlace.Head };

            Assert.Throws<InvalidArmorTypeException>(() => Methods.CheckArmor(hero, testArmor));
        }
        [Fact]
        public void Test_equip_right_weapon()
        {
            /// <summary>
            ///  Checks whether the weapon is equiped or not
            /// </summary>
            Character hero = new Warrior();
            Weapon testWeap = new Weapon("TestWeap", 1, 1, new WeaponAttributes(10, 2)) { Place = ItemPlace.Weapon, Type = Weaponkind.Axe };

            hero.currentWeapon = testWeap;
            string equiped = "";
        
            Assert.True(testWeap == hero.currentWeapon);
        }
        [Fact]
        public void Test_equip_right_armor()
        {
            /// <summary>
            ///  Checks whether the armor is equiped or not
            /// </summary>
            Character hero = new Warrior();
            Armor testArmor = new Armor("TestArmor", 1, 1, new ArmorAttributes(0, 2, 0)) { Type = ArmorType.Plate, Place = ItemPlace.Head };
           
           
                hero.currentHelm = testArmor;
             
            Assert.True(testArmor == hero.currentHelm);
        }

        [Fact]
        public void Test_warrior_dmg_lvl_1_no_weapon()
        {
            /// <summary>
            ///  Calculates the dmg at level one and no weapon by creating a hero, finding the primary attrubite and then calculating the dmg for that hero.
            ///  Checks with the actual dmg value if equal
            /// </summary>
            //arrange
            float str = 5;
            double actual = (1 + str / 100);

            //act
            Character hero = new Warrior();
            hero.FindPrimary();
            hero.Damage = (1 + (hero.PrimAttribute / 100));

            //assert
            Assert.Equal(hero.Damage, actual);
        }


        //fix rounding here
        [Fact]
        public void Test_warrior_dmg_lvl_1_with_weapon()
        {
            /// <summary>
            ///  Calculates the dmg at level one with a weapon by creating a hero, equiping an axe finding the primary attrubite and then calculating the dmg for that hero.
            ///  Checks with the actual dmg value if equal
            /// </summary>
           //arrange
            double actual1 = 7 * 1.1;
            float str = 5;
            double actual2 = actual1 * (1 + str / 100);

            //act
            Character hero = new Warrior();
            hero.currentWeapon = new Weapon("axe", 1, 1, new WeaponAttributes(7, 1.1));
            hero.FindPrimary();
            hero.Damage = hero.currentWeapon.DamageFromWeap * (1 + (hero.PrimAttribute / 100));

            //assert
            Assert.Equal(hero.Damage, actual2);
        }

        
        [Fact]
        public void Test_warrior_dmg_lvl_1_with_weapon_and_armor()
        {
            /// <summary>
            ///  Calculates the dmg at level one with a weapon by creating a hero, equiping an axe, equiping a plate armor, finding the primary attrubite and then calculating the dmg for that hero.
            ///  Checks with the actual dmg value if equal
            /// </summary>

            //arrange
            double actual1 = 7 * 1.1;
            float str = 6;
            double actual2 = actual1 * (1 + str / 100);

            //act
            Character hero = new Warrior();
            hero.currentWeapon = new Weapon("axe", 1, 1, new WeaponAttributes(7, 1.1));
            Armor ChestPlate = new Armor("plate body armor", 1, 1, new ArmorAttributes(1, 0, 0));
            hero.currentChest = ChestPlate;
            Methods.UpdateStats(hero, ChestPlate);
            hero.FindPrimary();
            hero.Damage = hero.currentWeapon.DamageFromWeap * (1 + (hero.PrimAttribute / 100));

            //assert
            Assert.Equal(hero.Damage, actual2);
        }
    }
}